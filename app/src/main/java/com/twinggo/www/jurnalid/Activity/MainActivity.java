package com.twinggo.www.jurnalid.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.twinggo.www.jurnalid.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.QuestionTitle1Container)
    RelativeLayout Container1;
    @BindView(R.id.QuestionTitle2Container)
    RelativeLayout Container2;
    @BindView(R.id.QuestionTitle3Container)
    RelativeLayout Container3;
    @BindView(R.id.Answer1)
    LinearLayout Answer1;
    @BindView(R.id.Answer2)
    LinearLayout Answer2;
    @BindView(R.id.Answer3)
    LinearLayout Answer3;
    @BindView(R.id.icon1)
    ImageView icon1;
    @BindView(R.id.icon2)
    ImageView icon2;
    @BindView(R.id.icon3)
    ImageView icon3;
    @BindView(R.id.gitlablink)
    TextView gitlablink;
    @BindView(R.id.answer3link)
    TextView answerlink;
    @BindDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp)
    Drawable downicon;
    @BindDrawable(R.drawable.ic_keyboard_arrow_up_black_24dp)
    Drawable upicon;
    Boolean StatAnswer1 = false;
    Boolean StatAnswer2 = false;
    Boolean StatAnswer3 = false;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener authListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(MainActivity.this, WelcomeActivity.class));
                    finish();
                }
            }
        };
        gitlablink.setMovementMethod(LinkMovementMethod.getInstance());
        answerlink.setMovementMethod(LinkMovementMethod.getInstance());
        collapse(Answer2);
        collapse(Answer1);
        collapse(Answer3);
    }


    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


    @OnClick(R.id.QuestionTitle1Container)
    public void onClickCointaner1 (View view){
        Log.e("1", "CLICK");
        if (StatAnswer1){
            StatAnswer1 = false;
            collapse(Answer1);
            icon1.setImageDrawable(downicon);
        }
        else {
            StatAnswer1 = true;
            expand(Answer1);
            icon1.setImageDrawable(upicon);
        }
    }


    @OnClick(R.id.QuestionTitle2Container)
    public void onClickCointaner2 (View view){
        if (StatAnswer2){
            StatAnswer2 = false;
            collapse(Answer2);
            icon2.setImageDrawable(downicon);

        }
        else {
            StatAnswer2 = true;
            expand(Answer2);
            icon2.setImageDrawable(upicon);
        }
    }

    @OnClick(R.id.QuestionTitle3Container)
    public void onClickCointaner3 (View view){
        if (StatAnswer3){
            StatAnswer3 = false;
            collapse(Answer3);
            icon3.setImageDrawable(downicon);

        }
        else {
            StatAnswer3 = true;
            expand(Answer3);
            icon3.setImageDrawable(upicon);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }


    @OnClick(R.id.button_logout)
    public void onLogout(View view) {
        auth.signOut();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



}
