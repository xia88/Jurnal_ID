package com.twinggo.www.jurnalid.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.twinggo.www.jurnalid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    @BindView(R.id.tools_container)
    LinearLayout toolcontainer;
    @BindView(R.id.emailRegister)
    EditText emailRegister;
    @BindView(R.id.passwordRegister)
    EditText passwordRegister;
    @BindView(R.id.repasswordRegister)
    EditText repasswordRegister;
    @BindView(R.id.loginRegister)
    TextView loginRegister;
    @BindView(R.id.container_register)
    RelativeLayout containerRegister;
    @BindView(R.id.registerRegister)
    Button register;
    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RegisterActivity.this, WelcomeActivity.class));
        finish();
    }



    private boolean isValidEmail(String email) {
        if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return true;
        }
        else {
            setSnackBar("Please Enter valid email address");
            return false;
        }

    }

    private boolean validatePassword() {
        if (passwordRegister.getText().toString().trim().isEmpty() || passwordRegister.getText().length()  < 7) {
            setSnackBar("Password min 8 Character");
            requestFocus(passwordRegister);
            return false;
        }

        else if (!passwordRegister.getText().toString().trim().equals(repasswordRegister.getText().toString())){
            setSnackBar("Password is not match");
            requestFocus(passwordRegister);
            return false;
        }
        else return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(containerRegister, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }


    @OnClick(R.id.registerRegister)
    public void onRegister(View view){
        toolcontainer.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        View focus = this.getCurrentFocus();
        if (focus != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (isValidEmail(emailRegister.getText().toString()) && validatePassword()){

            auth.createUserWithEmailAndPassword(emailRegister.getText().toString(), passwordRegister.getText().toString())
                    .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>(){

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (!task.isSuccessful()) {
                                if (task.getException().getMessage().isEmpty()){
                                    setSnackBar("Failed : " + "Something Wrong");
                                }
                                else {
                                    setSnackBar("Failed : " + task.getException().getMessage().toString());
                                    Log.e("Firebase error", task.getException().getMessage().toString());
                                }
                            } else {
                                startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                finish();
                            }
                            toolcontainer.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }

                    });
            toolcontainer.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }



    @OnClick(R.id.loginRegister)
    public void onLogin(View view){
        Intent nextactivity = new Intent(RegisterActivity.this, WelcomeActivity.class);
        startActivity(nextactivity);
        finish();
    }


    @OnClick(R.id.TosRegister)
    public void onTos(View view){
        Intent nextactivity = new Intent(RegisterActivity.this, TosActivity.class);
        startActivity(nextactivity);
    }

}
