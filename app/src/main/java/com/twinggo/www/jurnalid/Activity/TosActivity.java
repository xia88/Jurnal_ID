package com.twinggo.www.jurnalid.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.twinggo.www.jurnalid.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TosActivity extends AppCompatActivity {
    @BindView(R.id.webview_browser)
    WebView myWebview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tos);
        ButterKnife.bind(this);
        myWebview.loadUrl("https://www.jurnal.id/id/terms_and_condition");
    }
}
