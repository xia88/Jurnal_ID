package com.twinggo.www.jurnalid.Tools;

/**
 * Created by flix on 1/23/18.
 */

public class PurchaseModel {
    String pricepackage,adduser,addapi,total;
    public String getTotal() {
        return total;
    }

    public PurchaseModel(String pricepackage, String adduser, String addapi, String total) {

        this.pricepackage = pricepackage;
        this.adduser = adduser;
        this.addapi = addapi;
        this.total = total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPricepackage() {
        return pricepackage;
    }

    public void setPricepackage(String pricepackage) {
        this.pricepackage = pricepackage;
    }

    public String getAdduser() {
        return adduser;
    }

    public void setAdduser(String adduser) {
        this.adduser = adduser;
    }

    public String getAddapi() {
        return addapi;
    }

    public void setAddapi(String addapi) {
        this.addapi = addapi;
    }
}
