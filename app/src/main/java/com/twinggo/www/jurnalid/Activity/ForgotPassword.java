package com.twinggo.www.jurnalid.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.twinggo.www.jurnalid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPassword extends AppCompatActivity {

    @BindView(R.id.emailForgot)
    EditText emailForgot;
    @BindView(R.id.forgotForgot)
    Button forgotButton;
    @BindView(R.id.loginForgot)
    TextView loginForgot;
    @BindView(R.id.registerForgot)
    TextView registerForgot;
    @BindView(R.id.container_forgot)
    RelativeLayout container_forgot;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        auth = FirebaseAuth.getInstance();
        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ForgotPassword.this, WelcomeActivity.class));
        finish();
    }

    @OnClick(R.id.loginForgot)
    public void onLogin (View view){
        startActivity(new Intent(ForgotPassword.this, WelcomeActivity.class));
        finish();
    }

    @OnClick(R.id.registerForgot)
    public void onRegister (View view){
        startActivity(new Intent(ForgotPassword.this, RegisterActivity.class));
        finish();
    }

    @OnClick(R.id.forgotForgot)
    public void onForgot (View view){
        if (isValidEmail(emailForgot.getText().toString())){
            auth.sendPasswordResetEmail(emailForgot.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                setSnackBar("We have sent you instructions to reset your password!");
                            } else {
                                setSnackBar("Failed to send reset email!");
                            }
                        }
                    });
        }

    }


    private boolean isValidEmail(String email) {
        if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return true;
        }
        else {
            setSnackBar("Please Enter valid email address");
            return false;
        }

    }

    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(container_forgot, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

}
