package com.twinggo.www.jurnalid.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.twinggo.www.jurnalid.R;
import com.twinggo.www.jurnalid.Tools.PurchaseModel;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Purchase extends AppCompatActivity  implements AdapterView.OnItemSelectedListener {

    @BindView(R.id.dropdownPackage)
    Spinner packagemenu;
    @BindView(R.id.detail_button)
    TextView detailButton;
    @BindView(R.id.detail_container)
    LinearLayout detailContainer;
    @BindView(R.id.price_package)
    TextView pricepackage;
    @BindView(R.id.statusicon8)
    ImageView icon8;
    @BindView(R.id.statusicon9)
    ImageView icon9;
    @BindView(R.id.statusicon10)
    ImageView icon10;
    @BindView(R.id.statusicon11)
    ImageView icon11;
    @BindView(R.id.statusicon12)
    ImageView icon12;
    @BindView(R.id.statususer)
    TextView statususer;
    @BindDrawable(R.drawable.ic_check_circle_blue_24dp)
    Drawable blueicon;
    @BindDrawable(R.drawable.ic_check_circle_grey_24dp)
    Drawable greyicon;
    @BindView(R.id.plusapi)
    Button plusapi;
    @BindView(R.id.plususer)
    Button plususer;
    @BindView(R.id.minusapi)
    Button minusapi;
    @BindView(R.id.minususer)
    Button minususer;
    @BindView(R.id.countapi)
    TextView grandapi;
    @BindView(R.id.countuser)
    TextView granduser;
    @BindView(R.id.packagetype)
    TextView packagetype;
    @BindView(R.id.totalpackage)
    TextView totalpackage;
    @BindView(R.id.totalapi)
    TextView totalapi;
    @BindView(R.id.totaluser)
    TextView totaluser;
    @BindView(R.id.next_button)
    Button nextbutton;
    FirebaseAuth auth;
    DatabaseReference DatabaseUser;
    DatabaseReference PurchaseData;
    String UserID;
    int AddUser = 0;
    int AddApi  = 0;
    int TempApi = 0;
    int TempUser = 0;
    int TempPackage = 0;
    List<String> categories;
    DatabaseReference PurchaseStore;
    DatabaseReference UserStore;
    ValueEventListener DatabaseListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        UserID = auth.getCurrentUser().getUid();
        DatabaseUser =  FirebaseDatabase.getInstance().getReference("Purchase");
        categories = new ArrayList<String>();
        categories.add("Enteprise - Most Popular");
        categories.add("Pro");
        categories.add("Starter");
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        packagemenu.setAdapter(dataAdapter);
        packagemenu.setOnItemSelectedListener(this);
        collapse(detailContainer);


        DatabaseListener = new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("data",dataSnapshot.toString());
                if (dataSnapshot.exists()){
                    startActivity(new Intent(Purchase.this, MainActivity.class));
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                TempPackage = 250;
                pricepackage.setText("$" + TempPackage);
                totalpackage.setText("$" + TempPackage);
                icon8.setImageDrawable(blueicon);
                icon9.setImageDrawable(blueicon);
                icon10.setImageDrawable(blueicon);
                icon11.setImageDrawable(blueicon);
                icon12.setImageDrawable(blueicon);
                statususer.setText("5");
                break;
            case 1:
                TempPackage = 200;
                pricepackage.setText("$" + TempPackage);
                totalpackage.setText("$" + TempPackage);
                icon8.setImageDrawable(blueicon);
                icon9.setImageDrawable(blueicon);
                icon10.setImageDrawable(blueicon);
                icon11.setImageDrawable(greyicon);
                icon12.setImageDrawable(greyicon);
                statususer.setText("3");
                totalpackage.setText("$200");
                break;
            case 2:
                pricepackage.setText("$" + TempPackage);
                totalpackage.setText("$" + TempPackage);
                icon8.setImageDrawable(greyicon);
                icon9.setImageDrawable(greyicon);
                icon10.setImageDrawable(greyicon);
                icon11.setImageDrawable(greyicon);
                icon12.setImageDrawable(greyicon);
                statususer.setText("1");
                totalpackage.setText("$150");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }





    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


    @OnClick(R.id.detail_button)
    public void onDetail(View view) {
        if (detailButton.getText().toString().equals("Show Details Package") ) {
            detailButton.setText(R.string.HideDetails);
            expand(detailContainer);
        }
        else if (detailButton.getText().toString().equals("Hide Details Package"))
        {
            detailButton.setText(R.string.ShowDetails);
            collapse(detailContainer);
        }
    }



    public void CalculationAdditional (String type, String symbol){
        if (symbol.equals("-")){
            if (type.equals("user")){
                if (AddUser != 0){
                    AddUser = AddUser - 1;
                    granduser.setText(String.valueOf(AddUser));
                    TempUser = AddUser * 10;
                    totaluser.setText("$" + String.valueOf(TempUser));
                }
            }
            else if (type.equals("api")){
                if (AddApi != 0){
                    AddApi = AddApi - 1;
                    grandapi.setText(String.valueOf(AddApi));
                    TempApi = AddApi * 5;
                    totalapi.setText("$" + String.valueOf(TempApi));
                }
            }
        }
        else if (symbol.equals("+")){
            if (type.equals("user")){
                AddUser = AddUser + 1;
                TempUser = AddUser * 10;
                granduser.setText(String.valueOf(AddUser));
                totaluser.setText("$" + String.valueOf(TempUser));
            }
            else if (type.equals("api")){
                AddApi = AddApi + 1;
                grandapi.setText(String.valueOf(AddApi));
                TempApi = AddApi * 5;
                totalapi.setText("$" + String.valueOf(TempApi));
            }
        }
    }


    @OnClick (R.id.minusapi)
    public void onMinusApi(View view){
        CalculationAdditional("api","-");
    }

    @OnClick (R.id.plusapi)
    public void onPlusApi(View view){
        CalculationAdditional("api","+");
    }

    @OnClick (R.id.minususer)
    public void onMinusUser(View view){
        CalculationAdditional("user","-");
    }

    @OnClick (R.id.plususer)
    public void onPlusUser(View view){
        CalculationAdditional("user","+");
    }

    @OnClick(R.id.next_button)
    public void onNext(View view){
        UserStore = DatabaseUser.child(auth.getCurrentUser().getUid());
        UserStore.setValue(new PurchaseModel(packagemenu.getSelectedItem().toString(),String.valueOf(TempUser),String.valueOf(TempApi),String.valueOf(TempPackage+TempApi+TempUser)));
        PurchaseStore = FirebaseDatabase.getInstance().getReference("Purchase");
        PurchaseStore.child(auth.getCurrentUser().getUid()).addListenerForSingleValueEvent(DatabaseListener);
    }
}
