package com.twinggo.www.jurnalid.Activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.twinggo.www.jurnalid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends AppCompatActivity {

    @BindView(R.id.logo_container_Welcome)
    LinearLayout logocontainer;
    @BindView(R.id.desc_Welcome)
    TextView desc;
    @BindView(R.id.emailLoginWelcome)
    EditText email;
    @BindView(R.id.passwordLoginWelcome)
    EditText password;
    @BindView(R.id.loginLoginWelcome)
    Button loginbtn;
    @BindView(R.id.loginContainer)
    LinearLayout logincontainer;
    @BindView(R.id.welcomeContainer)
    RelativeLayout welcomeContainer;
    AnimatorSet hasloginAnim = new AnimatorSet();
    AnimatorSet notloginAnim = new AnimatorSet();
    ObjectAnimator logoAnim,descAnim,loginAnim;
    FirebaseAuth auth;
    DatabaseReference PurchaseStore;
    FirebaseAuth.AuthStateListener authListener;
    ValueEventListener DatabaseListener;
    Boolean frombutton = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        logoAnim = ObjectAnimator.ofFloat(logocontainer, "translationY",-250, 0);
        logoAnim.setDuration(1500);
        descAnim = ObjectAnimator.ofFloat(desc, "alpha", 0,1);
        descAnim.setDuration(1500);
        loginAnim = ObjectAnimator.ofFloat(logincontainer, "alpha", 0,1);
        logoAnim.setDuration(1500);
        hasloginAnim.play(descAnim).after(logoAnim);
        notloginAnim.play(loginAnim).after(descAnim).after(logoAnim);
        DatabaseListener = new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e("data",dataSnapshot.toString());
                if (dataSnapshot.exists()){
                    startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                    finish();
                }
                else{
                    startActivity(new Intent(WelcomeActivity.this, Purchase.class));
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (!frombutton){
                        hasloginAnim.start();
                    }
                    PurchaseStore = FirebaseDatabase.getInstance().getReference("Purchase");
                    PurchaseStore.child(auth.getCurrentUser().getUid()).addListenerForSingleValueEvent(DatabaseListener);
                }
                else {
                    notloginAnim.start();
                }
            }
        };

    }

    @OnClick(R.id.registerLogin)
    public void onRegister (View view){
        startActivity(new Intent(WelcomeActivity.this, RegisterActivity.class));
        finish();
    }

    @OnClick(R.id.forgotpasswordWelcome)
    public void onForgot (View view){
        startActivity(new Intent(WelcomeActivity.this, ForgotPassword.class));
        finish();
    }

    @OnClick(R.id.loginLoginWelcome)
    public void onLogin (View view){
        frombutton = true;
        loginbtn.setEnabled(false);
        View focus = this.getCurrentFocus();
        if (focus != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        if (isValidEmail(email.getText().toString()) && validatePassword()){
            if (auth.getCurrentUser() == null) {
                auth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnCompleteListener(WelcomeActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            setSnackBar( task.getException().getMessage().toString());
                        } else {

                        }
                    }
                });
            }
        }
        loginbtn.setEnabled(true);
    }


    private boolean isValidEmail(String email) {
        if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            return true;
        }
        else {
            setSnackBar("Please Enter valid email address");
            return false;
        }

    }

    private boolean validatePassword() {
        if (password.getText().toString().trim().isEmpty() || password.getText().length() <= 7) {
            setSnackBar("Password min 8 Character");
            requestFocus(password);
            return false;
        }
        else return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void setSnackBar(String snackTitle) {
        Snackbar snackbar = Snackbar.make(welcomeContainer, snackTitle, Snackbar.LENGTH_SHORT);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
